#include <SoftwareSerial.h>
SoftwareSerial receive(8, 7); //RX,TX
SoftwareSerial transmit(6, 9); //RX, TX

#include <SD.h>
#include <SPI.h>
#include <DS3231.h>
#define relay 4
#define relay2 5
File myFile;
DS3231 rtc(SDA, SCL);   // Code from the Demo Example of the DS3231 Library
int pinCS = 10; // Pin 10 on Arduino Uno
String hari;
String tgl;
String jam;

Time waktu;
//int jamAlarm, menitAlarm;

//int jamStartS1, menitStartS1, timerStatusS1, jamEndS1, menitEndS1;
//int jamStartS2, menitStartS2, timerStatusS2, jamEndS2, menitEndS2;
//String startTimeS1, endTimeS1, timerStatusS1, startTimeS2, endTimeS2,timerStatusS2;

float data, value;
float sensorValue, sensorValue1;
float tegangan;
float arus1;
float arus2;

String dataIn;
String dt[15];
int i;
int socketStatus1 = 2;
int socketStatus2 = 2;

boolean parsing = false;
int relay1Stat, relay2Stat;

char tombol = 6;
char tombol2 = 7;

float data1, value1;
float sensorValue2, sensorValue3;

float data2, value2;
float sensorValue4, sensorValue5;

float daya1, daya2, kWh;

long milisec = millis();
long time = milisec / 1000;

void setup() {
  Serial.begin(115200);  // setup koneksi serial
  receive.begin(115200);
  transmit.begin(115200);
  // sensorValue1 = data;
  pinMode(tombol, INPUT);
  pinMode(relay, OUTPUT);
  pinMode(tombol2, INPUT);
  pinMode(relay2, OUTPUT);
  digitalWrite(relay, HIGH);
  digitalWrite(relay2, HIGH);
  //  Serial.println("Ready...");
  // if (SD.begin()) {
  //  Serial.println("SD card is ready to use.");
  // } else {
  //   Serial.println("SD card initialization failed");
  //  return;
  // }
  // Initialize the rtc object
  rtc.begin();
  // The following lines can be uncommented to set the date and time
  //rtc.setDOW(SUNDAY);     // Set Day-of-Week to SUNDAY
  //rtc.setTime(16, 42, 300);     // Set the time to 12:00:00 (24hr format)
  //rtc.setDate(22, 7, 2018);   // Set the date to January 1st, 2014

}

void loop() {
  receiveData();
  relayControl();

//  if (timerStatusS1 == 1) {
//    setAlarmSocket1() ;
//  }
//  if (timerStatusS2 == 1) {
//    setAlarmSocket2() ;
//  }

  zmpt();
  acs1();
  acs2();
  RTCdanSD();
  daya();
  transmit.print("*");
  transmit.print(arus1);
  transmit.print(",");
  transmit.print(arus2);
  transmit.print(",");
  transmit.print(tegangan);
  transmit.print(",");
  transmit.print(daya1);
  transmit.print(",");
  transmit.print(daya2);
  transmit.print(",");
  transmit.print(kWh);
  transmit.print(",");
  transmit.print(tgl);
  transmit.print(",");
  transmit.print(jam);
//  transmit.print(",");
//  transmit.print(socketStatus1);
//  transmit.print(",");
//  transmit.print(socketStatus2);
  transmit.println("#");
  //        transmit.write("\n");

}

void parsingData() {
  int j = 0;
  //kirim data yang telah diterima sebelumnya
      Serial.print("data masuk : ");
      Serial.print(dataIn);
  Serial.print("\n");

  dt[j] = "";
  for (i = 1; i < dataIn.length(); i++) {
    if ((dataIn[i] == '#') || (dataIn[i] == ',')) {
      j++;
      dt[j] = "";
    } else {
      dt[j] = dt[j] + dataIn[i];
    }
  }
}
