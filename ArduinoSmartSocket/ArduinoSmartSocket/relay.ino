void relayControl() {
  if (relay1Stat == 1) {
    digitalWrite(relay, LOW);
  } else if (relay1Stat == 0) {
    digitalWrite(relay, HIGH);
  }

  if (relay2Stat == 1) {
    digitalWrite(relay2, LOW);
  } else if (relay2Stat == 0) {
    digitalWrite(relay2, HIGH);
  }
}
