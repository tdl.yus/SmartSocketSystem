void receiveData() {
  while (transmite.available() > 0) {
    char inChar = (char)transmite.read();
    dataIn += inChar;
    if (inChar == '\n') {
      parsing = true;
    }
  }
  if (parsing) {
    parsingData();
    parsing = false;
    dataIn = "";

    float arus1 = dt[0].toInt();
    float arus2 = dt[1].toInt();
    float tegangan = dt[2].toInt();
    float daya1 = dt[3].toInt();
    float daya2 = dt[4].toInt();
    kwatt = dt[5].toFloat();
    tanggal = dt[6];
    jm = dt[7];
    String statusS1 = dt[8];
    String statusS2 = dt[9];
    Serial.println();
    Serial.print("Arus 1 = ");
    Serial.println (arus1);
    Serial.print("Arus 2 = ");
    Serial.println (arus2);
    Serial.print("Tegangan = ");
    Serial.println (tegangan);
    Serial.print("Daya 1 = ");
    Serial.println (daya1);
    Serial.print("Daya 2 = ");
    Serial.println (daya2);
    Serial.print("kWh = ");
    Serial.println (kwatt);
    Serial.print("Tanggal = ");
    Serial.println (tanggal);
    Serial.print("Jam = ");
    Serial.println (jm);
    Serial.println();
    pushHistory();
//    delay(100);
    updateData(arus1, daya1, tegangan, arus2, daya2);
  }
}
