void postStatusS1(String status1) {
  Firebase.setString("smartSocket/statusSocket/socket1", status1);
  if (Firebase.failed()) {
    Serial.print("post status 1 failed:");
    Serial.println(Firebase.error());
    return;
  }
}

void postStatusS2(String status2){
  Firebase.setString("smartSocket/statusSocket/socket2", status2);
   if (Firebase.failed()) {
    Serial.print("post status 2 failed:");
    Serial.println(Firebase.error());
    return;
  }
}
