void getSocketStatus() {
  FirebaseObject object = Firebase.get("smartSocket/statusSocket");
  if (Firebase.failed()) {
    Serial.println("Firebase get failed");
    Serial.println(Firebase.error());
    digitalWrite(D0, LOW);
    return;
  } else {
    statSocket1 = object.getString("socket1");
    statSocket2 = object.getString("socket2");
  }
}
