void updateData(float arus1, float daya1, float tegangan, float arus2, float daya2) {
  updateSocket["arus"] = arus1;
  updateSocket["daya"] = daya1;
  updateSocket["tegangan"] = tegangan;
  Firebase.set("smartSocket/data/socket1", updateSocket);
  updateSocket["arus"] = arus2;
  updateSocket["daya"] = daya2;
  updateSocket["tegangan"] = tegangan;
  Firebase.set("smartSocket/data/socket2", updateSocket);
  //  Serial.println();
  //  Serial.println("Data update");

  //  updateSocket1.prettyPrintTo(Serial);
  //  Serial.println();
  if (Firebase.failed()) {
    Serial.print("setting time failed:");
    Serial.println(Firebase.error());
    Firebase.begin(FIREBASE_HOST);
    return;
  }
}
