void pushHistory() {
  sprintf(buffer, "history/%d", id);
  postHistory["kwh"] = kwatt;
  postHistory["tanggal"] = tanggal;
  postHistory["jam"] = jm;
  Firebase.set(buffer, postHistory);
  Firebase.set("smartSocket/log/logHistory/id", id);

  if (Firebase.failed()) {
    Serial.print("setting time failed:");
    Serial.println(Firebase.error());
    return;
  }
  id++;
}
